master_nodes = [
  {
    name            = "k3s-dd5-master"
    count           = 1
    bridge          = "vmbr161"
    network_model   = "virtio"
    cores           = 2
    sockets         = 1
    memory          = "2048"
    disk_size       = "50G"
  }
]

worker_nodes = [
  {
    name            = "k3s-dd5-main"
    count           = 2
    bridge          = "vmbr161"
    network_model   = "virtio"
    cores           = 2
    sockets         = 1
    memory          = "2048"
    disk_size       = "50G"
  }
]
