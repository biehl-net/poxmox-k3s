provider "proxmox" {
  pm_api_url      = var.proxmox_url
  pm_tls_insecure = true
}

module "master_nodes" {
  for_each = { for node in var.master_nodes : node.name => node }
  source   = "../modules/node_groups"

  node     = each.value

  clone_source    = var.clone_source
  searchdomain    = var.searchdomain

  local_exec      = "bash ${path.module}/dns-resolve.sh {name}.${var.searchdomain} && ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ${var.username} -i '{name}.${var.searchdomain},' ${path.module}/ansible/master-nodes.yml"
}

### Worker Nodes

module "worker_nodes" {

  depends_on = [module.master_nodes]

  for_each = { for node in var.worker_nodes : node.name => node }
  source   = "../modules/node_groups"

  node     = each.value

  clone_source    = var.clone_source
  searchdomain    = var.searchdomain

  local_exec      = "bash ${path.module}/dns-resolve.sh {name}.${var.searchdomain} && ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ${var.username} -i '{name}.${var.searchdomain},' -e 'k3s_url=\"${module.master_nodes[keys(module.master_nodes)[0]].master_url}\" k3s_token=\"${chomp(data.local_file.k3s_token.content)}\"' ${path.module}/ansible/worker-nodes.yml"
}

#### Get Kubeconfig Token From Master

resource "null_resource" "get_kubeconfig" {
  triggers  =  { always_run = "${timestamp()}" }
  provisioner "local-exec" {
    command = "scp -o StrictHostKeyChecking=no ${var.username}@${module.master_nodes[keys(module.master_nodes)[0]].master_url}:/var/k3s/k3s.yaml ${path.module}/kube.config.hidden"
  }
  depends_on = [module.master_nodes]
}

data "local_file" "kubeconfig" {
    filename = "${path.module}/kube.config.hidden"
  depends_on = [null_resource.get_kubeconfig]
}

#### Get K3S Token From Master

resource "null_resource" "get_k3s_token" {
  triggers  =  { always_run = "${timestamp()}" }
  provisioner "local-exec" {
    command = "scp -o StrictHostKeyChecking=no ${var.username}@${module.master_nodes[keys(module.master_nodes)[0]].master_url}:/var/k3s/node-token ${path.module}/k3s_token.hidden"
  }
  depends_on = [module.master_nodes]
}

data "local_file" "k3s_token" {
  filename = "${path.module}/k3s_token.hidden"
  depends_on = [null_resource.get_k3s_token]
}