#!/bin/bash

DNS_NAME=$1
if [ -z "$DNS_NAME" ]; then
    echo "DNS_NAME: First argument must be set."
    exit 0
fi

# Check if our DNS_NAME resolves
while [ -z "$(dig +short $DNS_NAME)" ]
do
    echo "DNS resolution failed, waiting"
    sleep 10
done
echo "DNS resolving to $(dig +short $DNS_NAME)"

# Check if host is up and we can ping it
while [ $(ping -c 1 $DNS_NAME &> /dev/null; echo $?) != 0 ]
do
    echo "Ping failed, waiting"
    sleep 10
done
echo "Host is up"

exit 0