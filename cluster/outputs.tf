output "kubeconfig" {
  value = chomp(data.local_file.kubeconfig.content)
}