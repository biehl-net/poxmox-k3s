# Setup
variable "proxmox_url" {
  description = "URL of Proxmox"
}

variable "searchdomain" {
  description = "DNS domain"
  type        = string
}

variable "username" {
  description = "Username for SSHing into newly created machines"
  type        = string
}

variable "clone_source" {
  description = "Proxmox VM clone source name"
  type        = string
  default     = "debian-cloud-init"
}

variable "master_nodes" {
  type = list(object({
    name    = string
    count   = number
    bridge  = string
    network_model = optional(string, "virtio")
    cores         = optional(number, 2)
    sockets       = optional(number, 2)
    memory        = optional(string, "4096")
    disk_size     = optional(string, "50G")
  }))
}

variable "worker_nodes" {
  type = list(object({
    name    = string
    count   = number
    bridge  = string
    network_model = optional(string, "virtio")
    cores         = optional(number, 2)
    sockets       = optional(number, 2)
    memory        = optional(string, "4096")
    disk_size     = optional(string, "50G")
  }))
}