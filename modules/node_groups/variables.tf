variable "node" {
  type = object({
    name    = string
    count   = number
    bridge  = string
    network_model = optional(string, "virtio")
    cores         = optional(number, 2)
    sockets       = optional(number, 2)
    memory        = optional(string, "4096")
    disk_size     = optional(string, "50G")
  })
}

variable "clone_source" {
  description = "Proxmox template or vm to clone new vm from"
  type        = string
}

variable "searchdomain" {
  description = "DNS domain"
  type        = string
  default     = "fatherbean.internal"
}

variable "k3s_url" {
  description = "URL of the master k3s node (should have been output when creating the master node)"
  type        = string
  default     = ""
}

variable "k3s_token" {
  description = "Token of the master k3s node (should have been output when creating the master node)"
  type        = string
  default     = ""
}

# Local Exec
variable "local_exec" {
  description = "Local exec script to run Ansible playbook"
  type        = string
}