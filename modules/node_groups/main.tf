module "node" {

  source   = "../node"
  count = var.node.count

  # General
  name          = "${var.node.name}-${count.index + 1}"
  clone         = var.clone_source

  # Network
  bridge        = var.node.bridge
  network_model = var.node.network_model

  # System
  cores         = var.node.cores
  sockets       = var.node.sockets
  memory        = var.node.memory

  # Disk
  size          = var.node.disk_size

  # Local exec (replacing {name} with replacement name)
  local_exec  = replace(var.local_exec, "{name}", "${var.node.name}-${count.index + 1}")
}

output "master_url" {
  value = module.node[0].master_url
}