resource "proxmox_vm_qemu" "virtual_machine" {

  name        = var.name
  desc        = var.name
  target_node = var.target_node

  clone = var.clone

  os_type      = var.os_type
  ipconfig0    = var.ipconfig0
  nameserver   = var.nameserver
  searchdomain = var.searchdomain

  cores        = var.cores
  sockets      = var.sockets
  memory       = var.memory

  cicustom = "vendor=local:snippets/vendor.yml" # Hardcoded on proxmox host /var/lib/vz/snippets

  disk {
    storage = var.disk
    type    = var.storage_type
    size    = var.size
  }

  network {
    bridge    = var.bridge
    firewall  = false
    link_down = false
    model     = var.network_model
  }

  lifecycle {
    ignore_changes = [
      ciuser,
      sshkeys,
      network,
    ]
  }

  provisioner "local-exec" {
    command = var.local_exec
  }
}

output "master_url" {
  value = "${var.name}.${var.searchdomain}"
}