# Setup
variable "name" {
  description  = "Name of new vm"
  type         = string
}

variable "clone" {
  description = "Proxmox template or vm to clone new vm from"
  type        = string
}

variable "target_node" {
  description = "Proxmox node to deploy vm to"
  type        = string
  default     = "moxx"
}

variable "os_type" {
  description = "Operating system type for the vm"
  type        = string
  default     = "cloud-init"
}

# Network
variable "nameserver" {
  description = "DNS nameserver IP"
  type        = string
  default     = "10.0.10.3"
}

variable "searchdomain" {
  description = "DNS domain"
  type        = string
  default     = "fatherbean.internal"
}

variable "bridge" {
  description = "Proxmox network bridge to use (e.g. vmbr161)"
  type        = string
}

variable "network_model" {
  description = "Virtual interface model type (e.g. virtio, e1000)"
  type        = string
  default     = "virtio"
}

variable "ipconfig0" {
  description = "Cloud init network settings"
  type        = string
  default     = "ip=dhcp"
}  

# System
variable "cores" {
  description = "CPU cores"
  type        = number
  default     = 4
}

variable "sockets" {
  description = "CPU sockets"
  type        = number
  default     = 1
}

variable "memory" {
  description = "System memory"
  type        = string
  default     = "4069"
}

# Disk
variable "disk" {
  description = "Proxmox storage disk to use"
  type        = string
  default     = "gring-vm-disk"
}

variable "storage_type" {
  description = "Storage type to use"
  type        = string
  default     = "scsi"
}

variable "size" {
  description = "VM disk size"
  type        = string
  default     = "30G"
}

# Local Exec
variable "local_exec" {
  description = "Local exec script to run Ansible playbook"
  type        = string
}