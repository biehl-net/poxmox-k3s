[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=biehl-net_poxmox-k3s&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=biehl-net_poxmox-k3s)
# K3S Kubernetes Proxmox VM Cluster

This project is used to spin up a k3s Kubernetes cluster onto Proxmox virtual machines. 

Using Terraform we create a k3s master node first and join our worker nodes once the master is online. This project relies on a proxmox cloud-init template image so you will need to create one of them first.

## IMPORTANT!
This project relies heavily on DDNS. You must have a DHCP server that is setup to push DDNS entries into your DNS server. All host access is done through their hostnames NOT by IP address.

[See here for more information](https://www.talk-about-it.ca/setup-bind9-with-isc-dhcp-server-dynamic-host-registration/)

## Installation

### Prerequisits

1. Install Proxmox API user:

    [I followed this guide to setup my Proxmox credentials](https://austinsnerdythings.com/2021/09/01/how-to-deploy-vms-in-proxmox-with-terraform/) (under heading "Determine Authentication Method (use API keys)")

2. Export Proxmox API credentials for use in Terraform:
    ```
    export PM_API_TOKEN_SECRET=1234
    export PM_API_TOKEN_ID='user@pam!terraform'
    ```

3. Create Proxmox cloud-init template:

    [Follow this guide to create a proxmox cloud-init template](https://docs.technotim.live/posts/cloud-init-cloud-image/)

    To note: You can edit any of the cloud-init settings of the template in the Proxmox GUI.

4. Create `snippets/vendor.yml` file

    Using Cloud init enables us to use a `vendor.yml` file to perform once off tasks to the newly created VM. You will need to manually create this file on your Proxmox host. The file path for this is `/var/lib/vz/snippets/vendor.yml`.

    I have used this file to remove an unneeded network interface and install qemu-guest-agent. 

    See here for an example: 
    ```
    user@proxmox# cat /var/lib/vz/snippets/vendor.yml
    #cloud-config
    runcmd:
    - ifdown eth0
    - rm /run/network/interfaces.d/ens18
    - ifup eth0
    - apt install -y qemu-guest-agent
    - systemctl start qemu-guest-agent
    ```

### Install The Cluster

Review the `nodes.auto.tfvars` file in the `cluster` directory and configure your master and worker nodes.

In the `cluster` directory run these terraform commands to create the cluster:
1. `terraform init`
2. `terraform plan -out plan`
3. `terraform apply plan`

Worker nodes will be automatically added to the cluster as they become available.

## Accessing the cluster

The kubeconfig file is output at the end of the apply command. Use this to modify the cluster with the `kubectl` command.

## Scaling the cluster

By modifying the `nodes.auto.tfvars` file we can scale our cluster accordingly.

To scale up increase the count, to scale down reduce it. 

**Note:** Scaling down has not been tested. The assumption is that the highest incremented node will be dropped. 
